#!/bin/bash

if [ -z $1 ] || [ $1 == "--help" ] ; then
	echo "usage wp_config.sh <db table prefix>"
	echo "example : wp_config.sh u4"
	exit
fi

FNAME="wp-config.php"
TBPREFIX=$1

echo "<?php" > $FNAME
echo "define('DB_NAME', \$_SERVER[\"DB1_NAME\"]);" >> $FNAME
echo "define('DB_USER', \$_SERVER[\"DB1_USER\"]);" >> $FNAME
echo "define('DB_PASSWORD', \$_SERVER[\"DB1_PASS\"]);" >> $FNAME
echo "define('DB_HOST', \$_SERVER[\"DB1_HOST\"].':'.\$_SERVER[\"DB1_PORT\"]);" >> $FNAME
echo "define('DB_CHARSET', 'utf8');" >> $FNAME
echo "define('DB_COLLATE', '');" >> $FNAME
echo $(curl -s https://api.wordpress.org/secret-key/1.1/salt/) >> $FNAME
echo "\$table_prefix  = '$TBPREFIX';" >> $FNAME
echo "define('WPLANG', '');" >> $FNAME
echo "define('WP_DEBUG', false);" >> $FNAME
echo "if ( !defined('ABSPATH') )" >> $FNAME
echo "	define('ABSPATH', dirname(__FILE__) . '/');" >> $FNAME
echo "require_once(ABSPATH . 'wp-settings.php');" >> $FNAME
